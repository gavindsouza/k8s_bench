# Kubernetes Bench

Operator and API to manage Frappe framework sites on Kubernetes.

## Production

Read [installation guide](https://k8s-bench.castlecraft.in/installation)

## Development

Read [development setup](https://k8s-bench.castlecraft.in/development)

#### License

MIT
