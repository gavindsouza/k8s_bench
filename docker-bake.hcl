variable "VERSION" {
  default = "latest"
}

variable "REGISTRY_NAME" {
  default = "registry.gitlab.com/castlecraft"
}

variable "OPERATOR_IMAGE_NAME" {
  default = "k8s_bench/operator"
}

variable "API_IMAGE_NAME" {
  default = "k8s_bench/api"
}

group "default" {
  targets = ["operator", "api"]
}

target "operator" {
    dockerfile = "images/operator.Dockerfile"
    tags = ["${REGISTRY_NAME}/${OPERATOR_IMAGE_NAME}:${VERSION}"]
}

target "api" {
    dockerfile = "images/api.Dockerfile"
    tags = ["${REGISTRY_NAME}/${API_IMAGE_NAME}:${VERSION}"]
}
