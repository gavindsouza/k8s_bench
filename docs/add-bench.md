## Prerequisites

To use CRD provided by fluxcd, install it using [`flux install --components=source-controller,helm-controller`](https://fluxcd.io/flux/faq/#can-i-use-flux-helmreleases-without-gitops) command before setting up the api. It will install enough resources to use the CRD without getting into GitOps.

To add bench (helm release), you need to add a source for helm chart. Either FluxCD's [`HelmRepository`](https://fluxcd.io/flux/components/source/helmrepositories/#examples) or [`GitRepository`](https://fluxcd.io/flux/components/source/gitrepositories/#examples) source is required.

For additional information on private sources refer official fluxcd [documentation](https://fluxcd.io/flux)

## Add Helm Release

Example `curl` command:


```shell
curl -X POST http://0.0.0.0:8000/flux/add-helmrelease \
    -H 'Content-Type: application/json' \
    -u 'admin:changeit' \
    -d '{
  "name": "frappe-bench",
  "namespace": "erpnext",
  "release_interval": "10m",
  "chart_interval": "10m",
  "chart": "./erpnext",
  "version": "6.0.0",
  "source_ref": {
        "kind": "GitRepository",
        "name": "frappe-helm",
        "namespace": "erpnext"
  },
  "values": {
      "image": { "repository": "custom/erpnext", "tag": "v4.2.0" },
      "persistence": { "worker": { "storageClass": "nfs" } },
      "jobs": { "configure": { "fixVolume": false }
    }
  },
  "remediate_last_failure": true
}'
```

- `name`: name of the helm release.
- `namespace`: namespace for deployment of the release.
- `release_interval`: interval to sync CRD with helm release
- `chart_interval`: interval to sync chart from chart source
- `chart`: name of chart in case of helm repo or path to chart directory in case of git repo.
- `version`: only used in case of chart source is helm repo.
- `source_ref`: can be `HelmRepository` or `GitRepository`. mention name and namespace of custom resource.
- `values`: json of values.yaml to override for helm chart.
- `remediate_last_failure`: [Configuring failure remediation](https://fluxcd.io/flux/components/helm/helmreleases/#configuring-failure-remediation)

Refer [API documentation](openapi.md) for details.
