## Prerequisites

API endpoint can initiate image build process using [Kaniko](https://github.com/GoogleContainerTools/kaniko). You need enough capacity on nodes to build the images. The images are pushed to registry using registry credential secret provided through `push_secret_name` body parameter.

To create secret execute following command:

```shell
kubectl -n builder create secret docker-registry ghcr-cred \
  --docker-server=ghcr.io \
  --docker-username=user \
  --docker-password=changeit
```

Note:

- Change the namespace to the one used to execute the build bench jobs.
- Change the secret name from `ghcr-cred` to the name of your choice.
- Change the `--docker-server` to your registry.
- Change the `--docker-user` to your registry username.
- Change the `--docker-password` to your registry password

## Execute Build Bench

Example `curl` command:

```shell
curl -X POST http://0.0.0.0:8000/jobs/build-bench \
    -H 'Content-Type: application/json' \
    -u 'admin:changeit' \
    -d '{
  "job_name": "build-custom-bench",
  "namespace": "builder",
  "push_secret_name": "ghcr-cred",
  "destination_image_name": "ghcr.io/user/repo/image:tag",
  "container_file_path": "images/custom/Containerfile",
  "frappe_path": "https://github.com/frappe/frappe",
  "frappe_branch": "version-14",
  "python_version": "3.10.9",
  "node_version": "16.19.0",
  "apps_json": [
    {"url":"https://github.com/frappe/payments","branch":"develop"},
    {"url":"https://user:password@git.example.com/org/project","branch":"version-14"}
  ],
  "git_repo_context": "github.com/castlecraft/frappe_containers"
}'
```

- `job_name`: name of the `Job` to be created.
- `namespace`: namespace for running the `Job`
- `push_secret_name`: name of the push secret created as prerequisite under the namespace mentioned above.
- `destination_image_name`: full name of the image including registry prefix, it will be used to push the image.
- `git_repo_context`: url of github repo, it is [build context used by Kaniko](https://github.com/GoogleContainerTools/kaniko#kaniko-build-contexts)
- `container_file_path`: location of Dockerfile or Containerfile specified in `git_repo_context`.
- `frappe_path`: https git repo source for frappe framework or fork.
- `frappe_branch`: git branch of above repo
- `python_version`: version of python to be used in image passed as build arg to Kaniko
- `node_version`: version of nodejs to be used in image passed as build arg to Kaniko
- `apps_json`: array of git repos and branches as app. e.g. format `[{"url":"https://github.com/user/repo","branch":"main"}]`
- `node_selector`: key value pair of node selector, used to schedule job on labelled nodes.

Refer [API documentation](openapi.md) for details.
