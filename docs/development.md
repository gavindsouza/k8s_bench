## Development

### Setup

Clone the repo and change directory.

```shell
git clone https://gitlab.com/castlecraft/k8s_bench
cd k8s_bench
```

Copy sample directories.

```shell
cp -R devcontainer .devcontainer
cp -R vscode .vscode
```

Re-open in VS Code devcontainer

### Setup cluster

Execute following script:

```shell
./tests/setup.sh
```

### Setup and activate python environment

```shell
python3 -m venv env
. ./env/bin/activate
pip install -e .
```

### Start Operator

```shell
. ./env/bin/activate
kopf run -A k8s_bench/kube_operator.py
```

### Start API

```shell
. ./env/bin/activate
uvicorn --host 0.0.0.0 --reload --port 8000 k8s_bench.main:app
```

### Pre-commit Check

```shell
. ./env/bin/activate
pre-commit run --color=always --all-files
```

### VS Code Debugging

To start python debugging, launch `K8s Bench API` or `K8s Bench Operator` from debugging panel instead of command line.

### Test using gitlab-runner

Commit code and execute

```shell
gitlab-runner exec docker --docker-privileged test_e2e_api
gitlab-runner exec docker --docker-privileged test_e2e_operator
```

### Test using scripts

Execute commands in following order to test api.

```shell
# Test setup
./tests/api/setup.sh

# RBAC in-cluster
./tests/setup_rbac_incluster.sh

# Start api in background
./env/bin/uvicorn --host 0.0.0.0 --port 8000 k8s_bench.main:app &

# Test API
./tests/api/execute.sh

# RBAC admin
./tests/setup_rbac_admin.sh

# Teardown
./tests/api/teardown.sh

# Kill uvicorn
kill %1
```

To test operator execute following.

```shell
# RBAC in cluster
./tests/setup_rbac_incluster.sh

# Start operator in background
./env/bin/kopf run -A k8s_bench/kube_operator.py &

# Test operator
./tests/operator/execute.sh

# kill operator
kill %1

# RBAC in cluster
./tests/setup_rbac_admin.sh
```

### Teardown

```shell
./tests/api/teardown.sh
```

Deletes flux `helmrelease`, uninstalls In-cluster NFS Server and uninstalls flux.
