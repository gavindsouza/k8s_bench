## Frappe Framework Apps

### Production configuration

In case of frappe framework apps, add `k8s_bench_url`, `k8s_bench_key` and `k8s_bench_secret` in `site_config.json` and use it to make python requests. You can use the internal kubernetes service url e.g. `http://manage-sites-k8s-bench.bench-system.svc.cluster.local:8000` instead of exposing the api if your frappe app also resides on same cluster. The service url will in case app is already deployed in cluster.

### Development setup

Use VS Code devcontainer for ease of use. Or use the `docker-compose.yml` file as mentioned in frappe_docker/docs. Your `.devcontainer/docker-compose.yml` should look like,

```yaml
version: "3.7"
services:
  mariadb:
    image: mariadb:10.6
    command:
      - --character-set-server=utf8mb4
      - --collation-server=utf8mb4_unicode_ci
      - --skip-character-set-client-handshake
      - --skip-innodb-read-only-compressed # Temporary fix for MariaDB 10.6
    environment:
      MYSQL_ROOT_PASSWORD: 123
    volumes:
      - mariadb:/var/lib/mysql

  redis-cache:
    image: redis:alpine

  redis-queue:
    image: redis:alpine

  redis-socketio:
    image: redis:alpine

  frappe:
    build: .
    command: ["tail", "-f", "/dev/null"]
    environment:
      - SHELL=/bin/bash
      - DOCKER_HOST=docker
    volumes:
      - ..:/workspace:cached
      # Enable if you require git cloning
      # - ${HOME}/.ssh:/home/frappe/.ssh
    working_dir: /workspace/development
    ports:
      - 8000-8005:8000-8005
      - 9000-9005:9000-9005

  # Kubernetes
  k3s:
    image: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/v1.23.4-k3s1:latest
    command:
      - server
      - --tls-san=k3s
      - --private-registry=/opt/custom/registries.yaml
      - --no-deploy=traefik
    volumes:
      - ./registries.yaml:/opt/custom/registries.yaml
      - k3s:/var/lib/rancher/k3s
    tmpfs:
      - /run
      - /var/run
    privileged: true

  # Container registry
  registry:
    image: registry:2
    volumes:
      - registry:/var/lib/registry
    environment:
      - REGISTRY_STORAGE_DELETE_ENABLED=true

  docker:
    image: docker:20.10.16-dind
    command:
      - dockerd
      - --host=0.0.0.0:2375
      - --insecure-registry=registry:5000
    privileged: true
    volumes:
      - docker:/var/lib/docker

volumes:
  mariadb:
  k3s:
  registry:
  docker:
```

Place `registries.yaml` in `.devcontainer` directory with following content:

```yaml
mirrors:
  "registry:5000":
    endpoint:
      - http://registry:5000
```

Place `Dockerfile` in `.devcontainer` directory with following content:

```Dockerfile
FROM frappe/bench:latest

# Install Tools
RUN sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
    curl -fsSL https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | sudo bash && \
    curl -fsSL https://fluxcd.io/install.sh | sudo bash && \
    curl -fsSL https://get.docker.com | sudo bash && \
    mkdir -p /home/frappe/.kube

ENV DOCKER_HOST=docker
ENV KUBECONFIG=/home/frappe/.kube/config
```

Once in frappe container you can execute `curl k3s:8081>$KUBECONFIG` to generate kubeconfig and start using `kubectl`, `helm`, `flux`.
