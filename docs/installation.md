# Installation

### Helm

```
helm repo add k8s-bench https://k8s-bench.castlecraft.in
helm upgrade --install \
  --create-namespace \
  --namespace bench-system \
  --set api.enabled=true \
  --set api.apiKey=admin \
  --set api.apiSecret=changeit \
  manage-sites k8s-bench/k8s-bench
```

Note: To use flux resources via api install flux first and then use `--set api.createFluxRBAC=true`. Find more about it in [Add Bench](add-bench.md).

Checkout helm chart [documentation](https://gitlab.com/castlecraft/k8s_bench/-/blob/main/helm-chart/README.md) for more configuration options.
