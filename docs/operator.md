Operator watches for jobs with following annotations and creates, updates or deletes ingress.

### Job Annotations

| Annotation                                       | Required | Description                                                        |
| ------------------------------------------------ | -------- | ------------------------------------------------------------------ |
| `k8s-bench.castlecraft.in/job-type`              | yes      | Used by operator to determine to create, update or delete ingress. Use one of `create-site`, `delete-site`, `update-site`.  |
| `k8s-bench.castlecraft.in/ingress-name`          | yes      | Used by operator to determine name of ingress to work upon.  |
| `k8s-bench.castlecraft.in/ingress-namespace`     | yes      | Used by operator to determine namespace of ingress to work upon.  |
| `k8s-bench.castlecraft.in/ingress-host`          | yes      | Used by operator to determine host for ingress to take action. |
| `k8s-bench.castlecraft.in/ingress-svc-name`      | yes      | Used by operator to determine service name for ingress to take action. |
| `k8s-bench.castlecraft.in/ingress-svc-port`      | yes      | Used by operator to determine service port for ingress to take action. |
| `k8s-bench.castlecraft.in/ingress-cert-secret`   | yes      | Used by operator to determine tls certificate secret for ingress certificate. |
| `k8s-bench.castlecraft.in/ingress-annotations`   | no       | Used by operator to determine annotations to be added on ingress during creation. |
| `k8s-bench.castlecraft.in/ingress-is-widcard`    | no       | Used by operator to determine if ingress is using wildcard certificate. |
| `k8s-bench.castlecraft.in/is-job-processed`      | no       | System annotation, DO NOT set manually. |
