
K8s-bench
===========

A Helm chart for operating frappe-bench hosted on Kubernetes


## Configuration

The following table lists the configurable parameters of the K8s-bench chart and their default values.

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `operator.image.repository` | container registry to pull the operator image from | `"registry.gitlab.com/castlecraft/k8s_bench/operator"` |
| `operator.image.pullPolicy` |  | `"IfNotPresent"` |
| `operator.image.tag` | Overrides operator image tag, default is chart appVersion. | `""` |
| `operator.resources` | Resource allocation for operator pod | `{}` |
| `operator.nodeSelector` | nodeSelector for operator pod | `{}` |
| `operator.tolerations` | tolerations for operator pod | `[]` |
| `operator.affinity` | affinity for operator pod | `{}` |
| `operator.podAnnotations` | pod annotations for operator pod | `{}` |
| `operator.podSecurityContext` | podSecurityContext for operator pod | `{}` |
| `operator.securityContext` | securityContext for operator container | `{}` |
| `api.enabled` | enable api service | `false` |
| `api.image.repository` | container registry to pull the api image from | `"registry.gitlab.com/castlecraft/k8s_bench/api"` |
| `api.image.pullPolicy` |  | `"IfNotPresent"` |
| `api.image.tag` | Overrides api image tag, default is chart appVersion. | `""` |
| `api.enableAuth` | Enable or disable Basic Auth for API | `true` |
| `api.apiKey` | API Key used to authenticate the endpoint using Basic Auth | `null` |
| `api.apiSecret` | API Secret used to authenticate the endpoint using Basic Auth | `null` |
| `api.service.type` | API Service type | `"ClusterIP"` |
| `api.service.port` | API Service port | `8000` |
| `api.ingress.enabled` | Enable public ingress for Bench API | `false` |
| `api.ingress.className` | Class name for ingress | `""` |
| `api.ingress.annotations` | Additional annotations for ingress | `{}` |
| `api.ingress.hosts` |  | `[{"host": "bench-api.local", "paths": [{"path": "/", "pathType": "ImplementationSpecific"}]}]` |
| `api.ingress.tls` | secretName and hosts. | `[]` |
| `api.createFluxRBAC` | Create RBAC for operating flux resources | `false` |
| `api.resources` | Resource allocation for api pod | `{}` |
| `api.autoscaling.enabled` | Enable creation of HorizontalPodAutoscaler | `false` |
| `api.autoscaling.minReplicas` | Number of minimum replicas for api deployment/pod | `1` |
| `api.autoscaling.maxReplicas` | Number of maximum replicas for api deployment/pod | `100` |
| `api.autoscaling.targetCPUUtilizationPercentage` | cpu utilization for scaling api deployment/pod | `80` |
| `api.nodeSelector` | nodeSelector for api pod | `{}` |
| `api.tolerations` | tolerations for api pod | `[]` |
| `api.affinity` | affinity for api pod | `{}` |
| `api.podAnnotations` | pod annotations for api pod | `{}` |
| `api.podSecurityContext` | podSecurityContext for api pod | `{}` |
| `api.securityContext` | securityContext for api container | `{}` |
| `imagePullSecrets` | Image pull secrets required for operator or api image | `[]` |
| `nameOverride` | helm chart name to override | `""` |
| `fullnameOverride` | helm chart full name to override | `""` |
| `serviceAccount.annotations` | Service account annotations | `{}` |
| `serviceAccount.name` | custom name for service account | `""` |
