bench-operator is installed on to the cluster.

{{- if .Values.api.enabled }}
bench-api is installed on the cluster.
service is created to access it on plain http via internal service

Find the api key using following command:

kubectl get deploy -n {{ .Release.Namespace }} {{ .Release.Name }}-k8s-bench-api -o jsonpath="{.spec.template.spec.containers[?(@.name=='bench-api')].env[?(@.name=='API_KEY')].value}"

Find the api secret using following command:

kubectl get secrets -n {{ .Release.Namespace }} {{ .Release.Name }}-k8s-bench -o jsonpath="{.data.api-secret}" | base64 -d
{{- if .Values.api.ingress.enabled }}
ingress is created to access it from public internet
{{- end }}
{{- end }}
