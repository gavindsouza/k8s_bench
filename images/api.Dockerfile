FROM python:3.10-alpine

ENV PYTHONUNBUFFERED 1

ENV REQUIREMENTS_FILE=requirements-api.txt

RUN addgroup -S craft -g 1000 && adduser -S craft -G craft -u 1000

COPY --chown=craft:craft . /opt/k8s_bench

WORKDIR /opt/k8s_bench

RUN python -m venv env && \
  ./env/bin/pip install --no-cache-dir -U pip .

ENV PATH="/opt/k8s_bench/env/bin:$PATH"

USER craft

CMD [ "uvicorn", "--host", "0.0.0.0", "--port", "8000", "k8s_bench.main:app" ]
