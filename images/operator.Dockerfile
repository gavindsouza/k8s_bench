FROM python:3.10-alpine

ENV PYTHONUNBUFFERED 1

ENV REQUIREMENTS_FILE=requirements-operator.txt

RUN addgroup -S craft -g 1000 && adduser -S craft -G craft -u 1000

COPY --chown=craft:craft . /opt/k8s_bench

WORKDIR /opt/k8s_bench

RUN python -m venv env && \
  ./env/bin/pip install --no-cache-dir -U pip .

ENV PATH="/opt/k8s_bench/env/bin:$PATH"

USER craft

CMD [ "kopf", "run", "-A", "--liveness=http://0.0.0.0:8080/healthz", "--verbose", "k8s_bench/kube_operator.py" ]
