import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasicCredentials
from kubernetes.client.exceptions import ApiException

from k8s_bench.api.auth import validate_basic_auth
from k8s_bench.api.dtos import NamespaceDto, SecretDto
from k8s_bench.client.core import (
    add_namespace,
    add_secret,
    delete_namespace,
    delete_secret,
    update_secret,
)

router = APIRouter(prefix="/core")


@router.post("/add-namespace")
def add_k8s_namespace(
    ns: NamespaceDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return add_namespace(name=ns.name)
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-namespace")
def delete_k8s_namespace(
    ns: NamespaceDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_namespace(name=ns.name)
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/add-secret")
def add_k8s_secret(
    secret: SecretDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return add_secret(
            name=secret.name,
            namespace=secret.namespace,
            string_data=secret.string_data,
            secret_type=secret.secret_type,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-secret")
def delete_k8s_secret(
    secret: SecretDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_secret(name=secret.name, namespace=secret.namespace)
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/update-secret")
def update_k8s_secret(
    secret: SecretDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return update_secret(
            name=secret.name,
            namespace=secret.namespace,
            string_data=secret.string_data,
            secret_type=secret.secret_type,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)
