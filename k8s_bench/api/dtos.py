import typing

from pydantic import BaseModel, Field

from k8s_bench.client.utils import SnapshotMode


class BenchCommandDto(BaseModel):
    job_name: str
    sites_pvc: str
    args: typing.List[str]
    command: typing.List[str] = None
    logs_pvc: str = None
    namespace: str = None
    image_pull_secrets: typing.List[dict[str, str]] = None
    image: str = None
    annotations: dict = None
    node_selector: dict = None
    resources: dict = None


class FrappeAppDto(BaseModel):
    url: str
    branch: str


class HostAliasDto(BaseModel):
    ip: str
    hostnames: typing.List[str]


class BuildBenchDto(BaseModel):
    job_name: str
    namespace: str
    push_secret_name: str
    destination_image_name: str
    container_file_path: str = None
    frappe_path: str = None
    frappe_branch: str = None
    python_version: str = None
    node_version: str = None
    apps_json: typing.List[FrappeAppDto] = None
    git_repo_context: str = None
    node_selector: dict = None
    insecure_registry: bool = None
    host_aliases: typing.List[HostAliasDto] = None
    snapshot_mode: SnapshotMode = None
    use_new_run: bool = None
    cache: bool = None
    resources: dict = None


class FetchJobStatusDto(BaseModel):
    job_name: str
    namespace: str


class SourceRefDto(BaseModel):
    kind: str
    name: str
    namespace: str


class AddBenchDto(BaseModel):
    name: str
    namespace: str
    release_interval: str
    chart_interval: str
    chart: str = Field(
        description="""path to chart in case of git repo, e.g. "./erpnext"
         or chart name in case of helm repo, e.g. "erpnext"."""  #
    )
    source_ref: SourceRefDto
    values: dict
    remediate_last_failure: bool
    version: str = None
    timeout: str = None


class BenchDto(BaseModel):
    name: str
    namespace: str


class NamespaceDto(BaseModel):
    name: str


class SecretDto(BaseModel):
    name: str
    namespace: str
    string_data: dict[str, str] = None
    secret_type: str = None


class HelmSourceDto(BaseModel):
    name: str
    namespace: str
    kind: str
    interval: str = None
    url: str = None
    secret_ref: str = None


class IngressDto(BaseModel):
    name: str
    namespace: str
    host: str = None
    service_name: str = None
    service_port: int = None
    cert_secret_name: str = None
    is_wildcard: bool = None
    annotations: dict[str, str] = None


class EnvVarDto(BaseModel):
    name: str
    value: str = None
    value_from: dict = None


class CronJobDto(BaseModel):
    name: str
    namespace: str
    sites_pvc: str
    cronstring: str
    image: str = None
    args: typing.List[str] = None
    backoff_limit: int = 0
    command: typing.List[str] = None
    logs_pvc: str = None
    image_pull_secrets: typing.List[dict[str, str]] = None
    annotations: dict = None
    node_selector: dict = None
    resources: dict = None
    env_vars: typing.List[EnvVarDto] = None


class MetaDataDto(BaseModel):
    name: str
    namespace: str
