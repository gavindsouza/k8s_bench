import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasicCredentials
from kubernetes.client.exceptions import ApiException

from k8s_bench.api.auth import validate_basic_auth
from k8s_bench.api.dtos import IngressDto
from k8s_bench.client.ingress import (
    get_ingress_status,
    create_ingress,
    patch_ingress,
    delete_ingress,
)

router = APIRouter(prefix="/ingress")


@router.get("/get-status")
def get_k8s_ingress_status(
    name: str,
    namespace: str,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return get_ingress_status(
            ingress_name=name,
            namespace=namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/create")
def create_k8s_ingress_status(
    ingress: IngressDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return create_ingress(
            ingress_name=ingress.name,
            namespace=ingress.namespace,
            host=ingress.host,
            service_name=ingress.service_name,
            service_port=ingress.service_port,
            cert_secret_name=ingress.cert_secret_name,
            is_wildcard=ingress.is_wildcard,
            annotations=ingress.annotations,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/patch")
def patch_k8s_ingress_status(
    ingress: IngressDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return patch_ingress(
            ingress_name=ingress.name,
            namespace=ingress.namespace,
            service_name=ingress.service_name,
            service_port=ingress.service_port,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete")
def delete_k8s_ingress_status(
    ingress: IngressDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_ingress(
            ingress_name=ingress.name,
            namespace=ingress.namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)
