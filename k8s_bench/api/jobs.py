import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasicCredentials
from kubernetes.client.exceptions import ApiException

from k8s_bench.api.auth import validate_basic_auth
from k8s_bench.api.dtos import (
    BenchCommandDto,
    BuildBenchDto,
    FetchJobStatusDto,
)
from k8s_bench.client.jobs import (
    bench_command_job,
    build_bench_image,
    delete_job,
    get_job_status,
)

router = APIRouter(prefix="/jobs")


@router.post("/bench-command")
def execute_bench_command(
    bench_command: BenchCommandDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return bench_command_job(
            job_name=bench_command.job_name,
            sites_pvc=bench_command.sites_pvc,
            args=bench_command.args,
            command=bench_command.command,
            logs_pvc=bench_command.logs_pvc,
            namespace=bench_command.namespace,
            image_pull_secrets=bench_command.image_pull_secrets,
            image=bench_command.image,
            annotations=bench_command.annotations,
            node_selector=bench_command.node_selector,
            resources=bench_command.resources,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/build-bench")
def execute_build_bench(
    build_bench: BuildBenchDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    data = build_bench.parse_obj(build_bench)
    apps_json = build_bench.dict().get("apps_json")
    host_aliases = build_bench.dict().get("host_aliases")
    try:
        return build_bench_image(
            job_name=data.job_name,
            namespace=data.namespace,
            push_secret_name=data.push_secret_name,
            destination_image_name=data.destination_image_name,
            container_file_path=data.container_file_path,
            frappe_path=data.frappe_path,
            frappe_branch=data.frappe_branch,
            python_version=data.python_version,
            node_version=data.node_version,
            apps_json=apps_json,
            git_repo_context=data.git_repo_context,
            node_selector=data.node_selector,
            insecure_registry=data.insecure_registry,
            host_aliases=host_aliases,
            use_new_run=data.use_new_run,
            snapshot_mode=data.snapshot_mode,
            cache=data.cache,
            resources=data.resources,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.get("/get-status")
def fetch_job_status(
    job_name: str,
    namespace: str,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return get_job_status(
            job_name=job_name,
            namespace=namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-job")
def delete_k8s_job(
    payload: FetchJobStatusDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_job(
            name=payload.job_name,
            namespace=payload.namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)
