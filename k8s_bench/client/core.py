from k8s_bench.client.utils import load_config, to_dict
from kubernetes import client


def add_namespace(name):
    load_config()
    body = client.V1Namespace(metadata=client.V1ObjectMeta(name=name))
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.create_namespace(body=body)
    return to_dict(api_response)


def delete_namespace(name):
    load_config()
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.delete_namespace(name=name)
    return to_dict(api_response)


def add_secret(
    name: str,
    namespace: str,
    string_data: dict,
    secret_type: str = None,
):
    load_config()
    body = client.V1Secret(
        metadata=client.V1ObjectMeta(name=name, namespace=namespace),
        string_data=string_data,
        type=secret_type,
    )
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.create_namespaced_secret(
        namespace=namespace,
        body=body,
    )
    return to_dict(api_response)


def delete_secret(name: str, namespace: str):
    load_config()
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.delete_namespaced_secret(
        name=name,
        namespace=namespace,
    )
    return to_dict(api_response)


def update_secret(
    name: str,
    namespace: str,
    string_data: dict,
    secret_type: str = None,
):
    load_config()
    body = client.V1Secret(
        metadata=client.V1ObjectMeta(name=name, namespace=namespace),
        string_data=string_data,
        type=secret_type,
    )
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.patch_namespaced_secret(
        name=name,
        namespace=namespace,
        body=body,
    )
    return to_dict(api_response)
