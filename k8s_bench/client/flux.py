from k8s_bench.client.utils import load_config, to_dict
from kubernetes import client


def add_flux_helm_release(
    name: str,
    namespace: str,
    release_interval: str,
    chart_interval: str,
    chart: str,
    version: str,
    source_ref: dict,
    values: dict,
    remediate_last_failure: bool,
    timeout: str = None,
):
    load_config()
    crd = client.CustomObjectsApi()
    body = {
        "apiVersion": "helm.toolkit.fluxcd.io/v2beta1",
        "kind": "HelmRelease",
        "metadata": {
            "name": name,
            "namespace": namespace,
        },
        "spec": {
            "timeout": timeout,
            "interval": release_interval,
            "chart": {
                "spec": {
                    "chart": chart,
                    "version": version,
                    "sourceRef": source_ref,
                    "interval": chart_interval,
                },
            },
            "upgrade": {
                "remediation": {
                    "remediateLastFailure": remediate_last_failure,
                },
            },
            "values": values,
        },
    }

    api_response = crd.create_namespaced_custom_object(
        group="helm.toolkit.fluxcd.io",
        version="v2beta1",
        namespace=namespace,
        plural="helmreleases",
        body=body,
        pretty=True,
    )

    return to_dict(api_response)


def delete_flux_helm_release(
    name: str,
    namespace: str,
):
    load_config()
    crd = client.CustomObjectsApi()

    api_response = crd.delete_namespaced_custom_object(
        group="helm.toolkit.fluxcd.io",
        version="v2beta1",
        namespace=namespace,
        plural="helmreleases",
        name=name,
    )

    return to_dict(api_response)


def get_helmsource_status(name: str, namespace: str, kind: str):
    load_config()
    crd = client.CustomObjectsApi()
    plural = None
    if kind == "HelmRepository":
        plural = "helmrepositories"
    elif kind == "GitRepository":
        plural = "gitrepositories"
    else:
        raise client.ApiException("Invalid Flux HelmSource", status=500)

    api_response = crd.get_namespaced_custom_object_status(
        group="source.toolkit.fluxcd.io",
        version="v1beta2",
        namespace=namespace,
        plural=plural,
        name=name,
    )
    return to_dict(api_response)


def add_flux_helm_source(
    name: str,
    namespace: str,
    kind: str,
    interval: str,
    url: str,
    secret_ref: str = None,
):
    load_config()
    crd = client.CustomObjectsApi()
    plural = None
    if kind == "HelmRepository":
        plural = "helmrepositories"
    elif kind == "GitRepository":
        plural = "gitrepositories"
    else:
        raise client.ApiException("Invalid Flux HelmSource", status=500)

    body = {
        "apiVersion": "source.toolkit.fluxcd.io/v1beta2",
        "kind": kind,
        "metadata": {
            "name": name,
            "namespace": namespace,
        },
        "spec": {
            "interval": interval,
            "url": url,
        },
    }

    if secret_ref:
        body["spec"]["secretRef"] = {"name": secret_ref}

    api_response = crd.create_namespaced_custom_object(
        group="source.toolkit.fluxcd.io",
        version="v1beta2",
        namespace=namespace,
        plural=plural,
        body=body,
        pretty=True,
    )

    return to_dict(api_response)


def delete_flux_helm_source(
    name: str,
    namespace: str,
    kind: str,
):
    load_config()
    crd = client.CustomObjectsApi()
    plural = None
    if kind == "HelmRepository":
        plural = "helmrepositories"
    elif kind == "GitRepository":
        plural = "gitrepositories"
    else:
        raise client.ApiException("Invalid Flux HelmSource", status=500)
    body = {
        "apiVersion": "source.toolkit.fluxcd.io/v1beta2",
        "metadata": {
            "name": name,
            "namespace": namespace,
        },
    }

    api_response = crd.delete_namespaced_custom_object(
        name=name,
        group="source.toolkit.fluxcd.io",
        version="v1beta2",
        namespace=namespace,
        plural=plural,
        body=body,
    )

    return to_dict(api_response)


def get_helmrelease_status(name: str, namespace: str):
    load_config()
    crd = client.CustomObjectsApi()
    api_response = crd.get_namespaced_custom_object_status(
        group="helm.toolkit.fluxcd.io",
        version="v2beta1",
        namespace=namespace,
        plural="helmreleases",
        name=name,
    )
    return to_dict(api_response)


def update_flux_helm_source(
    name: str,
    namespace: str,
    kind: str,
    interval: str,
    url: str = None,
    secret_ref: str = None,
):
    load_config()
    crd = client.CustomObjectsApi()
    plural = None
    if kind == "HelmRepository":
        plural = "helmrepositories"
    elif kind == "GitRepository":
        plural = "gitrepositories"
    else:
        raise client.ApiException("Invalid Flux HelmSource", status=500)

    body = {
        "apiVersion": "source.toolkit.fluxcd.io/v1beta2",
        "kind": kind,
        "metadata": {
            "name": name,
            "namespace": namespace,
        },
        "spec": {
            "interval": interval,
        },
    }

    if url:
        body["spec"]["url"] = url

    if secret_ref:
        body["spec"]["secretRef"] = {"name": secret_ref}
    else:
        body["spec"]["secretRef"] = None

    api_response = crd.patch_namespaced_custom_object(
        group="source.toolkit.fluxcd.io",
        version="v1beta2",
        namespace=namespace,
        plural=plural,
        name=name,
        body=body,
    )

    return to_dict(api_response)


def update_flux_helm_release(
    name: str,
    namespace: str,
    release_interval: str,
    chart_interval: str,
    chart: str,
    version: str,
    source_ref: dict,
    values: dict,
    remediate_last_failure: bool,
    timeout: str = None,
):
    load_config()
    crd = client.CustomObjectsApi()
    body = {
        "apiVersion": "helm.toolkit.fluxcd.io/v2beta1",
        "kind": "HelmRelease",
        "metadata": {
            "name": name,
            "namespace": namespace,
        },
        "spec": {
            "timeout": timeout,
            "interval": release_interval,
            "chart": {
                "spec": {
                    "chart": chart,
                    "version": version,
                    "sourceRef": source_ref,
                    "interval": chart_interval,
                },
            },
            "upgrade": {
                "remediation": {
                    "remediateLastFailure": remediate_last_failure,
                },
            },
            "values": values,
        },
    }

    api_response = crd.patch_namespaced_custom_object(
        group="helm.toolkit.fluxcd.io",
        version="v2beta1",
        namespace=namespace,
        plural="helmreleases",
        name=name,
        body=body,
    )

    return to_dict(api_response)
