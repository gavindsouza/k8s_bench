import logging

from fastapi import FastAPI

from k8s_bench import __version__ as k8s_bench_version
from k8s_bench.api.filters import EndpointFilter
from k8s_bench.api.core import router as core
from k8s_bench.api.flux import router as flux
from k8s_bench.api.jobs import router as jobs
from k8s_bench.api.ingress import router as ingress
from k8s_bench.api.cronjobs import router as cronjobs

# Filter out /healthz
logging.getLogger("uvicorn.access").addFilter(EndpointFilter())

app = FastAPI(
    title="K8s Bench",
    docs_url=None,
    redoc_url="/docs",
    version=k8s_bench_version,
)

app.include_router(core)
app.include_router(jobs)
app.include_router(flux)
app.include_router(ingress)
app.include_router(cronjobs)


@app.get("/healthz")
def healthcheck():
    return {"status": "ok"}
