#!/bin/bash

set -e

# Uninstall frappe-bench Helm Release
echo -e "\033[1mUninstall frappe-bench Helm Release\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/flux/delete-helmrelease \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "frappe-bench",
        "namespace": "erpnext"
      }' | jq .
kubectl delete helmreleases.helm.toolkit.fluxcd.io -n erpnext frappe-bench --wait --timeout=10m
echo -e ""

# Create reg-cred secret in builder namespace
echo -e "\033[1mDelete reg-cred secret from builder namespace\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/delete-secret \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "reg-cred",
        "namespace": "builder"
      }' | jq .
echo -e ""

# Create reg-cred secret in erpnext namespace
echo -e "\033[1mDelete reg-cred secret from erpnext namespace\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/delete-secret \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "reg-cred",
        "namespace": "erpnext"
      }' | jq .
echo -e ""

# Delete helm source
echo -e "\033[1mDelete helm source\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/flux/delete-helmsource \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "frappe",
        "namespace": "builder",
        "kind": "HelmRepository"
      }' | jq .
echo -e ""

echo -e "\033[1mDelete Namespaces\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/delete-namespace \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{"name":"erpnext"}' | jq .
echo -e ""
curl -fsS -X POST http://0.0.0.0:8000/core/delete-namespace \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{"name":"builder"}' | jq .
echo -e ""
