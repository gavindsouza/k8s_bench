#!/bin/bash

set -e

# Create namespaces
echo -e "\033[1mCreate Namespaces\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/add-namespace \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{"name":"erpnext"}' | jq .
echo -e ""
curl -fsS -X POST http://0.0.0.0:8000/core/add-namespace \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{"name":"builder"}' | jq .
echo -e ""

if [ -z "${CI_REGISTRY}" ]; then
	CI_REGISTRY=registry:5000
	export CI_REGISTRY
fi

if [ -z "${CI_JOB_TOKEN}" ]; then
	CI_JOB_TOKEN=password
	export CI_JOB_TOKEN
fi

if [ -z "${CI_REGISTRY_USER}" ]; then
	CI_REGISTRY_USER=gitlab-ci-token
	export CI_REGISTRY_USER
fi

AUTH=$(echo -n "${CI_REGISTRY_USER}:${CI_JOB_TOKEN}" | base64)
export AUTH
DOCKERCONFIGJSON=("{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_JOB_TOKEN}\",\"auth\":\"${AUTH}\"}}}")
export DOCKERCONFIGJSON

# Create reg-cred secret in builder namespace
echo -e "\033[1mCreate reg-cred secret in builder namespace\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/add-secret \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
        \"name\": \"reg-cred\",
        \"namespace\": \"builder\",
        \"string_data\": {
					\".dockerconfigjson\": $(echo "${DOCKERCONFIGJSON}" | jq '. | tostring')
        },
        \"secret_type\": \"kubernetes.io/dockerconfigjson\"
      }" | jq .
echo -e ""

# Create reg-cred secret in erpnext namespace
echo -e "\033[1mCreate reg-cred secret in erpnext namespace\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/add-secret \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
        \"name\": \"reg-cred\",
        \"namespace\": \"erpnext\",
        \"string_data\": {
					\".dockerconfigjson\": $(echo "${DOCKERCONFIGJSON}" | jq '. | tostring')
        },
        \"secret_type\": \"kubernetes.io/dockerconfigjson\"
      }" | jq .

echo -e "\033[1mCreate temp-cred secret in erpnext namespace\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/add-secret \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
        \"name\": \"temp-cred\",
        \"namespace\": \"erpnext\",
        \"string_data\": {
					\"secretKey\": \"secretValue\"
        }
      }" | jq .

# Update temp-cred secret in erpnext namespace
echo -e "\033[1mUpdate temp-cred secret in erpnext namespace\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/update-secret \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
        \"name\": \"temp-cred\",
        \"namespace\": \"erpnext\",
        \"string_data\": {
					\"secretKey\": \"changedValue\"
        }
      }" | jq .

CHANGED_SECRET=$(kubectl get secret -n erpnext temp-cred -o jsonpath="{.data.secretKey}" | base64 -d)
export CHANGED_SECRET

if [ "${CHANGED_SECRET}" != "changedValue" ]; then
	echo "incorrect secret value after change"
	exit 1
fi

# Create temp-cred secret in builder namespace
echo -e "\033[1mDelete temp-cred secret from builder namespace\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/delete-secret \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "temp-cred",
        "namespace": "erpnext"
      }' | jq .
echo -e ""
