#!/bin/bash

set -e

IMAGE_NAME=registry.gitlab.com/castlecraft/k8s_bench/custom-image:latest
if [ -z "${CI_REGISTRY}" ]; then
	IMAGE_NAME=registry:5000/custom-image:latest
fi
export IMAGE_NAME

echo -e "\033[1mCreate cronjob/backup-erpnext\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/cronjobs/add \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
  \"name\": \"backup-erpnext\",
  \"namespace\": \"erpnext\",
  \"sites_pvc\": \"frappe-bench-erpnext\",
  \"cronstring\": \"0 */12 * * *\",
  \"args\": [
    \"bench\",
    \"--site\",
    \"all\",
    \"backup\"
  ],
  \"image\": \"${IMAGE_NAME}\",
  \"image_pull_secrets\": [{\"name\": \"reg-cred\"}]
}" | jq .
echo -e ""

echo -e "\033[1mCheck if cronjob/backup-erpnext exists\033[0m"
curl -fsS "http://0.0.0.0:8000/cronjobs/get-status?name=backup-erpnext&namespace=erpnext" \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' | jq .
echo -e ""

echo -e "\033[1mDelete cronjob/backup-erpnext\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/cronjobs/delete \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
  \"name\": \"backup-erpnext\",
  \"namespace\": \"erpnext\"
}" | jq .
kubectl get cronjob -n erpnext backup-erpnext 2>/dev/null ||
	export CRONJOB_DELETED=1 && echo "cronjob/backup-erpnext deleted"
if [ -z "${CRONJOB_DELETED}" ]; then
	echo "Error: cronjob not deleted"
	exit 1
fi
echo -e ""
