#!/bin/bash

set -e

KUBECONFIG="${HOME}"/.kube/config
export KUBECONFIG

wait4ports tcp://0.0.0.0:8000

./tests/api/healthcheck.sh

./tests/api/core.sh

./tests/api/jobs_build_bench.sh

./tests/api/flux_new.sh

./tests/api/flux_get.sh

./tests/api/jobs_bench_command.sh

./tests/api/flux_update.sh

./tests/api/cronjobs.sh

./tests/api/ingress.sh

./tests/api/cleanup.sh
