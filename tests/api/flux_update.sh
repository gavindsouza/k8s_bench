#!/bin/bash

set -e

IMAGE_NAME=registry.gitlab.com/castlecraft/k8s_bench/custom-image
if [ -z "${CI_REGISTRY}" ]; then
	IMAGE_NAME=registry:5000/custom-image
fi
export IMAGE_NAME

# Update helm release source
echo -e "\033[1mCheck update helm release source\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/flux/update-helmsource \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "frappe",
        "namespace": "builder",
        "kind": "HelmRepository",
        "interval": "10m0s"
      }' | jq .

# Add Flux Bench
echo -e "\033[1mUpdate Flux Bench\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/flux/update-helmrelease \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
  \"name\": \"frappe-bench\",
  \"namespace\": \"erpnext\",
  \"release_interval\": \"60s\",
  \"chart_interval\": \"60s\",
  \"chart\": \"erpnext\",
  \"source_ref\": {
        \"kind\": \"HelmRepository\",
        \"name\": \"frappe\",
        \"namespace\": \"builder\"
  },
  \"values\": {
    \"image\": { \"repository\": \"${IMAGE_NAME}\", \"tag\": \"latest\" },
    \"persistence\": { \"worker\": { \"storageClafss\": \"nfs\" } },
    \"jobs\": {
        \"configure\": { \"fixVolume\": false },
        \"migrate\": { \"enabled\": true, \"siteName\": \"all\" }
    },
    \"imagePullSecrets\": [{ \"name\": \"reg-cred\" }]
  },
  \"remediate_last_failure\": true,
  \"timeout\": \"15m\"
}" | jq .

echo -e ""
echo -e "\033[1mWaiting for helm release to be ready after update\033[0m"
kubectl -n erpnext wait --timeout=1800s --for=condition=ready helmreleases frappe-bench

echo -e "\033[1mGet Flux Bench Status after update\033[0m"
curl -fsS "http://0.0.0.0:8000/flux/get-helmrelease-status?name=frappe-bench&namespace=erpnext" \
	-u 'admin:changeit' | jq .
echo -e ""

echo -e "\033[1mPort forward svc/frappe-bench-erpnext on 8080 after update\033[0m"
kubectl port-forward -n erpnext svc/frappe-bench-erpnext 8080:8080 &

# wait for port 8080 to be running
wait4ports tcp://0.0.0.0:8080

echo -e "\033[1mPing frappe site after update\033[0m"
curl -fsSL -H "Host: frappe.localhost" http://localhost:8080/api/method/ping | jq .
echo -e ""

# kill kubectl port-forward
kill %1
