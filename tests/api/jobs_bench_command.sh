#!/bin/bash

set -e

IMAGE_NAME=registry.gitlab.com/castlecraft/k8s_bench/custom-image
if [ -z "${CI_REGISTRY}" ]; then
	IMAGE_NAME=registry:5000/custom-image
fi
export IMAGE_NAME

# Initiate Execute Bench Command
echo -e "\033[1mInitiate Execute Bench Command\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/jobs/bench-command \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
  \"job_name\": \"create-frappe-local\",
  \"sites_pvc\": \"frappe-bench-erpnext\",
  \"args\": [
    \"bench\",
    \"new-site\",
    \"--install-app=castlecraft\",
    \"--admin-password=admin\",
    \"--db-root-password=changeit\",
    \"--force\",
    \"--no-mariadb-socket\",
    \"frappe.localhost\"
  ],
  \"command\": null,
  \"logs_pvc\": null,
  \"namespace\": \"erpnext\",
  \"image_pull_secrets\": [{\"name\": \"reg-cred\"}],
  \"image\": \"${IMAGE_NAME}:latest\"
}" | jq .

echo -e ""
echo -e "\033[1mWaiting for create-frappe-local to complete\033[0m"
JOBUUID=$(kubectl -n erpnext get job create-frappe-local -o "jsonpath={.metadata.labels.controller-uid}")
export JOBUUID
echo "Job uuid ${JOBUUID}"
PODNAME=$(kubectl -n erpnext get pod -l controller-uid="${JOBUUID}" -o name)
export PODNAME
echo "Pod name ${PODNAME}"
kubectl -n erpnext wait --timeout=900s --for=condition=ready "${PODNAME}"
echo "Logs..."
kubectl -n erpnext logs job/create-frappe-local -f
kubectl -n erpnext wait --timeout=900s --for=condition=complete job/create-frappe-local

echo -e "\033[1mPort forward svc/frappe-bench-erpnext on 8080\033[0m"
kubectl port-forward -n erpnext svc/frappe-bench-erpnext 8080:8080 &

# wait for port 8080 to be running
wait4ports tcp://0.0.0.0:8080

echo -e "\033[1mPing frappe site\033[0m"
curl -fsSL -H "Host: frappe.localhost" http://localhost:8080/api/method/ping | jq .
echo -e ""

# kill kubectl port-forward
kill %1

# Delete Jobs from erpnext namespace
echo -e "\033[1mDelete Jobs from erpnext namespace\033[0m"
kubectl delete jobs -n erpnext --all
