#!/bin/bash

# Setup flux
# https://fluxcd.io/flux/faq/#can-i-use-flux-helmreleases-without-gitops
echo -e "\033[1mInstall Flux HelmController and SourceController\033[0m"
flux install --components=source-controller,helm-controller

# Install in-cluster NFS Server
echo -e "\033[1mInstall In Cluster NFS Server\033[0m"
kubectl create namespace nfs
helm repo add nfs-ganesha-server-and-external-provisioner https://kubernetes-sigs.github.io/nfs-ganesha-server-and-external-provisioner
helm upgrade --install -n nfs in-cluster --version 1.5.0 \
	nfs-ganesha-server-and-external-provisioner/nfs-server-provisioner \
	--set 'storageClass.mountOptions={vers=4.1}' \
	--set persistence.enabled=true \
	--set persistence.size=8Gi
