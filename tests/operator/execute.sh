#!/bin/bash

set -e

KUBECONFIG="${HOME}"/.kube/config
export KUBECONFIG

# Create namespace operator
kubectl create namespace operator

# Create job with annotations to create ingress
kubectl -n operator apply -f tests/operator/dummy-new-ingress-job.yaml

# Watch for ingress creation
./tests/operator/ingress_watch.py create

# Create job with annotations to patch ingress
kubectl -n operator apply -f tests/operator/dummy-patch-ingress-job.yaml

# Watch for ingress patch
./tests/operator/ingress_watch.py update

# Create job with annotations to delete ingress
kubectl -n operator apply -f tests/operator/dummy-delete-ingress-job.yaml

# Watch for ingress deletion
./tests/operator/ingress_watch.py delete

# Delete all jobs from operator namespace
kubectl -n operator delete job --all

# Delete namespace operator
kubectl delete namespace operator
