#!./env/bin/python
import argparse
import time
import logging
import sys
from kubernetes import config
from k8s_bench.client.ingress import get_ingress
from kubernetes.client.exceptions import ApiException

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("watch-ingress")


class IngressNotUpdatedException(Exception):
    pass


def parse_args():
    parser = argparse.ArgumentParser("watch")
    parser.add_argument("--name", default="frappe-localhost")
    parser.add_argument("--namespace", default="operator")
    subparsers = parser.add_subparsers(dest="command")
    subparsers.add_parser("create", help="watch ingress creation")
    subparsers.add_parser("update", help="watch ingress update")
    subparsers.add_parser("delete", help="watch ingress deletion")
    return parser.parse_args()


def watch_ingress(
    args: argparse.Namespace,
    iterations=30,
    wait_interval=1,
    fail_on_404=False,
):
    ingress = None
    i = 0
    while i < iterations:
        try:
            ingress = get_ingress(args.name, args.namespace)
            break
        except ApiException as e:
            if e.status == 404 and fail_on_404:
                raise e
            if e.status != 404:
                raise e
            log.info(
                f"ingress/{args.name} not found in namespace/{args.namespace}"
            )  # noqa: E501
        time.sleep(wait_interval)
        i = i + 1

    return ingress


def watch_ingress_create(args: argparse.Namespace):
    ingress = watch_ingress(args)
    if not ingress:
        log.error(f"ingress/{args.name} not found")
        sys.exit(1)

    creation = ingress["metadata"]["creationTimestamp"]
    log.info(
        f"ingress/{args.name} in namespace/{args.namespace} created at {creation}"  # noqa: E501
    )


def watch_ingress_update(
    args: argparse.Namespace,
    iterations=30,
    wait_interval=1,
):
    i = 0
    not_update_msg = (
        f"ingress/{args.name} in namespace/{args.namespace} not updated"  # noqa: E501
    )
    while i < iterations:
        ingress = get_ingress(args.name, args.namespace)
        if ingress:
            gen = ingress.get("metadata", {}).get("generation")
            if gen > 1:
                log.info(
                    f"ingress/{args.name} in namespace/{args.namespace} updated"  # noqa: E501
                )
                break
            else:
                log.info(not_update_msg)
        i = i + 1
        time.sleep(wait_interval)

    if ingress and ingress.get("metadata", {}).get("generation") == 1:
        log.error(not_update_msg)
        sys.exit(1)


def watch_ingress_delete(
    args: argparse.Namespace,
    iterations=30,
    wait_interval=1,
):
    i = 0
    ingress = None
    not_deleted_msg = (
        f"ingress/{args.name} in namespace/{args.namespace} not deleted"  # noqa: E501
    )
    deleted_msg = (
        f"ingress/{args.name} in namespace/{args.namespace} deleted"  # noqa: E501
    )
    while i < iterations:
        try:
            ingress = get_ingress(args.name, args.namespace)
            if ingress:
                log.info(not_deleted_msg)
        except ApiException as e:
            if e.status == 404:
                log.info(deleted_msg)
                ingress = None
                break

        i = i + 1
        time.sleep(wait_interval)

    if ingress:
        log.error(not_deleted_msg)
        sys.exit(1)


def main():
    args = parse_args()
    config.load_kube_config()
    if args.command == "create":
        watch_ingress_create(args)
    elif args.command == "update":
        watch_ingress_update(args)
    elif args.command == "delete":
        watch_ingress_delete(args)


if __name__ == "__main__":
    main()
